import Define from './Define'
class WebSocketManager {
  constructor(){
    this.state='disconnected'
    this.init = this.init.bind(this)
    this.onopen= this.onopen.bind(this)
    this.onerror= this.onerror.bind(this)
    this.onclose= this.onclose.bind(this)
    this.onmessage= this.onmessage.bind(this)
    this.onDataChange= this.onDataChange.bind(this)
    this.removeListener= this.removeListener.bind(this)
    this.processCallback = this.processCallback.bind(this)
    this.callbackPool=[]

    this.init()
  }
  init(){
    // this.ws&&this.ws.close()
    this.state='connecting'
    this.ws = new WebSocket(`${Define.serverAddr}`)
    this.ws.onopen=this.onopen
    this.ws.onerror=this.onerror
    this.ws.onclose=this.onclose
    this.ws.onmessage=this.onmessage
  }
  processCallback(data){
    if(Array.isArray(this.callbackPool)){
      this.callbackPool.forEach((cb)=>{
        setTimeout(()=>{cb(data);});
      });
    }
  }
  onopen(){
    this.state='connected'
    this.processCallback({state:this.state})
  }
  onerror(event){
    // console.log('onerror',event)
  }
  onclose(){
    this.state='disconnected'
    this.processCallback({state:this.state})
    setTimeout(()=>{
      this.init()
    },1000)
  }
  onmessage(event){
    var dataObj;
    try{
      dataObj = JSON.parse(event.data)
    }catch(err){
      // console.log()
    }
    if(dataObj){
      this.processCallback(dataObj)
    }
  }
  onDataChange(callback){
    this.removeListener(callback);
    this.callbackPool.push(callback);
  }
  removeListener(callback){
    const idx = this.callbackPool.indexOf(callback);
    if(idx>=0){
      this.callbackPool.splice(idx, 1);
    }
  }
}

export default new WebSocketManager()
