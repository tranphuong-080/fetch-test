import React from 'react'

import { Container, Row, Col, Table } from 'reactstrap';

import webSocketManager from './WebSocketManager'

import Util from './Util'

class App extends React.Component {
  constructor(){
    super();
    this.state = {
      buyList:[],
      sellList:[],
      lastUpdated:undefined
    }
    this.onDataChange = this.onDataChange.bind(this)
  }
  onDataChange(data){
    if(data.buyList&&data.sellList){
      this.setState({
        buyList:data.buyList.sort((a,b)=>b.price-a.price),
        sellList:data.sellList.sort((a,b)=>a.price-b.price),
        lastUpdated: new Date()
      })
    }else if(data.state){
      this.setState({})
    }
  }
  render(){
    return (
      <Container>
        <br/>
        <br/>
          <Row>
            <Col sm='2'>
              <h4>Connection:</h4>
            </Col>
            <Col sm='2'>
              <h4 style={{color:webSocketManager.state==='connected'?'green':'red'}}>{webSocketManager.state}</h4>
            </Col>
            <Col sm={{size:2,offset:2}}>
              <h4>Last updated:</h4>
            </Col>
            <Col sm='4'>
              <h4>{this.state.lastUpdated?Util.date2String(this.state.lastUpdated,'HH:MM:SS dd/mm/yyyy'):'Wait for first event'}</h4>
            </Col>
          </Row>
        <br/>
        <Row>
          <h4>Data base on Bittrex ETH-BTC:</h4>
        </Row>
        <br/>
        <Row>
          <Col sm={{size:5,offset:0}} className='border rounded' >
            <Row>
              <h2>Bid</h2>
            </Row>
            <Row>
              <Table bordered>
                <thead>
                  <tr>
                    <th style={{width:'50%'}}>
                      Size
                    </th>
                    <th style={{width:'50%'}}>
                      Bid
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.buyList.map(c=>(
                    <tr>
                      <td>
                        {c.size}
                      </td>
                      <td>
                        {c.price}
                      </td>
                    </tr>
                  ))}
                  <tr>
                    <th>Total Amount</th>
                    <th>{this.state.buyList.reduce((t,c)=>t+(c.size*c.price),0)}</th>
                  </tr>
                </tbody>
              </Table>
            </Row>
          </Col>
          <Col sm={{size:5,offset:1}} className='border rounded'>
            <Row>
              <h2>Ask</h2>
            </Row>
            <Row>
              <Table bordered>
                <thead>
                  <tr>
                    <th style={{width:'50%'}}>
                      Ask
                    </th>
                    <th style={{width:'50%'}}>
                      Size
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.sellList.map(c=>(
                    <tr>
                      <td>
                        {c.price}
                      </td>
                      <td>
                        {c.size}
                      </td>
                    </tr>
                  ))}
                  <tr>
                    <th>Total Size</th>
                    <th>{this.state.sellList.reduce((t,c)=>t+(c.size),0)}</th>
                  </tr>
                </tbody>
              </Table>
            </Row>
          </Col>
        </Row>
      </Container>
    )
  }
  componentDidMount(){
    webSocketManager.onDataChange(this.onDataChange)
  }
}

export default App;
