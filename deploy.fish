#!/usr/bin/fish

function build_web
  node --version
  cd web
  yarn install
  yarn build
  cd ..
end

if [ (count $argv) = 0 ]
  #statements
  build_web
else
  for x in $argv
    if [ $x = web ]
      build_web
    end
  end
end

env (cat .env)  docker-compose -f docker-compose.yml config > production.yml

eval (docker-machine env mkp-server)
# docker-compose -f production.yml build --no-cache $argv
docker-compose -f production.yml up --build -d $argv
