const WebSocket = require('ws');
const bittrexManager = require('./BittrexManager');

const TOTAL_BUY_SELL = 22;
const TOTAL_AMOUNT_BID = 5;
const TOTAL_SIZE_ASK = 150;

const wss = new WebSocket.WebSocketServer({ port: 3030 });

var lastData={};
var lastRetData={};

function processData(data){
  var retData;
  if(data&&data.delta&&data.delta.high&&data.delta.low&&
        (data.delta.high!==data.delta.low)){
    lastData=data;
    retData={};
    const high = parseFloat(data.delta.high);
    const low = parseFloat(data.delta.low);
    // random price of buy and sell
    const randomArr = [];
    for (let i = 0; i < TOTAL_BUY_SELL; i++) {
      randomArr[i] = parseFloat((low + (Math.random() * (high-low))).toFixed(8)) ;
    }
    randomArr.sort((a, b)=>a-b);
    const buyPriceList=randomArr.slice(0, 11);
    const sellPriceList=randomArr.slice(11);

    // Process buy list
    var idxLeft =Array(buyPriceList.length).fill()
      .map((x, i)=>i); // use for random order process in buyPriceList
    const buyList=[];
    var currentAmountBidLeft = TOTAL_AMOUNT_BID;
    for (let i = 0; i < buyPriceList.length; i++) {
      let r = Math.round(Math.random()*(idxLeft.length-1) );
      let idxR = idxLeft[r];
      let sizeR = parseFloat( ( (Math.random() * ((currentAmountBidLeft / idxLeft.length) * (idxLeft.length*0.6)) ) / buyPriceList[idxR] )
        .toFixed(2+Math.round(Math.random()*6)) ) ;
      idxLeft.splice(r, 1);
      currentAmountBidLeft-=(sizeR*buyPriceList[idxR]);
      buyList[idxR]={
        size: sizeR,
        price: buyPriceList[idxR]
      };
    }
    retData.buyList=buyList;


    // Process sell list
    idxLeft =Array(sellPriceList.length).fill()
      .map((x, i)=>i); // use for random order process in buyPriceList
    const sellList=[];
    var currentSizeLeft = TOTAL_SIZE_ASK;
    for (let i = 0; i < sellPriceList.length; i++) {
      let r = Math.round(Math.random()*(idxLeft.length-1) );
      let idxR = idxLeft[r];
      let sizeR = parseFloat( (Math.random() * ((currentSizeLeft / idxLeft.length) * (idxLeft.length*0.6)) ).toFixed(2+Math.round(Math.random()*6) )) ;
      idxLeft.splice(r, 1);
      currentSizeLeft-=sizeR;
      sellList[idxR]={
        size: sizeR,
        price: sellPriceList[idxR]
      };
    }

    retData.sellList=sellList;
  }
  return retData;
}

var timeoutEnsure5s;

function onDataChange(data){
  // console.log('onDataChange', data);
  // console.log('wss.clients', wss.clients);
  clearTimeout(timeoutEnsure5s);
  const retData = processData(data);
  lastRetData=retData;
  const retDataString = JSON.stringify(retData);
  wss.clients.forEach((ws)=>{
    setTimeout(()=>{
      ws.send(retDataString);
    });
  });

  timeoutEnsure5s=setTimeout(()=>{
    onDataChange(lastData);
  }, 5000);
}

bittrexManager.onDataChange(onDataChange);

wss.on('connection', (ws) => {
  // console.log('on connection', ws);
  ws.send(JSON.stringify({hello: 'hello'}) ); //JSON.stringtify()'hello'
  ws.send(JSON.stringify(lastRetData) );
  // clients.set(ws, metadata);
});
