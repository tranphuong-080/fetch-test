const signalR = require('signalr-client');
const zlib = require('zlib');
const crypto = require('crypto');
const uuid = require('uuid');

const url = 'wss://socket-v3.bittrex.com/signalr';
const hub = ['c3'];

const apikey = '9ffce0e55c5f497f9db49f2303b5580b';
const apisecret = '0495753d581e46099ef0dd48a843ab44';

var resolveInvocationPromise = () => { };

class BittrexManager{
  constructor(){
    this.callbackPool=[];
    this.messageReceived = this.messageReceived.bind(this);
    this.init();
  }
  init(){
    if(this.client){this.client.end();}
    this.connect()
      .then((client)=>{
        this.client = client;
        if (apisecret) {
          return this.authenticate(this.client);
        }else{
          return Promise.reject(new Error('Authentication skipped because API key was not provided'));
        }
      })
      .then(()=>{
        this.subscribe(this.client);
      });
  }
  connect() {
    return new Promise((resolve) => {
      console.log('Connecting');
      const client = new signalR.client(url, hub); //eslint-disable-line new-cap
      client.serviceHandlers.messageReceived = this.messageReceived;
      client.serviceHandlers.connected = () => {
        console.log('Connected');
        return resolve(client);
      };
      client.serviceHandlers.bindingError = () =>{
        console.log('bindingError');
        setTimeout(()=>{
          this.init();
        }, 1000);
      };
      client.serviceHandlers.disconnected = () =>{
        console.log('Disconnected');
        setTimeout(()=>{
          this.init();
        }, 1000);
      };
    });
  }
  authenticate(client) {
    const timestamp = new Date().getTime();
    const randomContent = uuid.v4();
    const content = `${timestamp}${randomContent}`;
    const signedContent = crypto.createHmac('sha512', apisecret)
      .update(content)
      .digest('hex')
      .toUpperCase();

    this.invoke(client, 'authenticate',
      apikey,
      timestamp,
      randomContent,
      signedContent)
      .then((response)=>{
        if (response.Success) {
          console.log('Authenticated');
        }
        else {
          console.log('Authentication failed: ' + response.ErrorCode);
        }
      });
  }
  subscribe(client) {
    const channels = [
      'heartbeat',
      'candle_ETH-BTC_MINUTE_5',
      // 'market_summary_ETH-BTC'
    ];

    this.invoke(client, 'subscribe', channels)
      .then((response)=>{
        if(!Array.isArray(response)) {return;}
        for (var i = 0; i < channels.length; i++) {
          if (response[i] && response[i].Success) {
            console.log('Subscription to "' + channels[i] + '" successful');
          }
          else {
            console.log('Subscription to "' + channels[i] + '" failed: ' + response[i].ErrorCode);
          }
        }
      });
  }
  invoke(client, method, ...args) {
    return new Promise((resolve, reject) => {
      resolveInvocationPromise = resolve; // Promise will be resolved when response message received

      client.call(hub[0], method, ...args)
        .done((err) => {
          if (err) { return reject(err); }
        });
    });
  }
  messageReceived(message) {
    const data = JSON.parse(message.utf8Data);
    if (data.R) {
      resolveInvocationPromise(data.R);
    }
    else if (data.M) {
      data.M.forEach((m) => {
        if (m.A) {
          if (m.A[0]) {
            const b64 = m.A[0];
            const raw = new Buffer.from(b64, 'base64'); //eslint-disable-line new-cap

            zlib.inflateRaw(raw, (err, inflated) => {
              if (!err) {
                const json = JSON.parse(inflated.toString('utf8'));
                // console.log(m.M + ': ');
                if(Array.isArray(this.callbackPool)){
                  this.callbackPool.forEach((cb)=>{
                    setTimeout(()=>{cb(json);});
                  });
                }
              }
            });
          }
          else if (m.M == 'heartbeat') {
            // console.log('\u2661');
          }
          else if (m.M == 'authenticationExpiring') {
            console.log('Authentication expiring...');
            this.authenticate(this.client);
          }
        }
      });
    }
  }
  onDataChange(callback){
    this.removeListener(callback);
    this.callbackPool.push(callback);
  }
  removeListener(callback){
    const idx = this.callbackPool.indexOf(callback);
    if(idx>=0){
      this.callbackPool.splice(idx, 1);
    }
  }
}

module.exports = new BittrexManager();
